<?php

namespace Drupal\oembed_resource;

use Drupal\Core\Form\FormStateInterface;

/**
 * The PerformanceFormHandler class.
 */
class PerformanceFormHandler {

  /**
   * Alter Drupal\system\Form\PerformanceForm.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the id of the form.
   */
  public function alterForm(array &$form, FormStateInterface $form_state, $form_id) {
    // Add a submit handler.
    $form['clear_cache']['oembed_resource']['#submit'][] = [
      PerformanceFormHandler::class, 'invalidateCache',
    ];
  }

  /**
   * Invalidates cache tags for oEmbed media entities.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function invalidateCache(array &$form, FormStateInterface $form_state) {
    \Drupal::service('oembed_resource.helper')->invalidateAllOembedMediaEntities();
  }

}
