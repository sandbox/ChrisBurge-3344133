<?php

namespace Drupal\oembed_resource;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies services.
 */
class OembedResourceServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('media.oembed.resource_fetcher');
    // Replace 'cache.default' service with 'cache.oembed_resource' service.
    foreach ($definition->getArguments() as $index => $argument) {
      if ($argument->__toString() == 'cache.default') {
        $definition->replaceArgument($index, new Reference('cache.oembed_resource'));
        break;
      }
    }

    $definition = $container->getDefinition('media.oembed.url_resolver');
    // Replace 'cache.default' service with 'cache.oembed_resource' service.
    foreach ($definition->getArguments() as $index => $argument) {
      if ($argument->__toString() == 'cache.default') {
        $definition->replaceArgument($index, new Reference('cache.oembed_resource'));
        break;
      }
    }
  }

}
