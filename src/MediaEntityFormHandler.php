<?php

namespace Drupal\oembed_resource;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pcb\Cache\PermanentBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The MediaEntityFormHandler class.
 */
class MediaEntityFormHandler implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The oEmbed resource cache backend.
   *
   * @var \Drupal\pcb\Cache\PermanentBackendInterface
   */
  protected $cache;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The Helper service.
   *
   * @var \Drupal\oembed_resource\HelperInterface
   */
  protected $helper;

  /**
   * MediaEntityFormHandler constructor.
   *
   * @param \Drupal\pcb\Cache\PermanentBackendInterface $cache
   *   The oEmbed resource cache backend.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   * @param \Drupal\oembed_resource\HelperInterface $helper
   *   The Helper service.
   */
  public function __construct(PermanentBackendInterface $cache, DateFormatterInterface $date_formatter, AccountProxyInterface $account, HelperInterface $helper) {
    $this->cache = $cache;
    $this->dateFormatter = $date_formatter;
    $this->account = $account;
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.oembed_resource'),
      $container->get('date.formatter'),
      $container->get('current_user'),
      $container->get('oembed_resource.helper')
    );
  }

  /**
   * Alter Drupal\media\MediaForm.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the id of the form.
   */
  public function alterForm(array &$form, FormStateInterface $form_state, $form_id) {
    /** @var \Drupal\media\MediaInterface $entity */
    $entity = $form_state->getFormObject()->getEntity();
    if ($entity->isNew()) {
      return;
    }

    if ($this->helper->isOembed($entity)) {
      if ($timestamp = $this->helper->getCacheTimestampFromEntity($entity)) {
        $cache_created = $this->dateFormatter->format($timestamp, 'short', '', $this->account->getTimeZone());
      }
      else {
        $cache_created = $this->t('N/A: Resource is not cached.');
      }

      $form['cache'] = [
        '#type' => 'details',
        '#title' => $this->t('oEmbed Resource Cache'),
      ];
      $form['cache']['cache-timestamp'] = [
        '#markup' => $this->t('<p>Resource Fetch Date: <span id="cache-timestamp">@cache_created</span></p>',
          ['@cache_created' => $cache_created]
        ),
      ];
      $form['cache']['refetch'] = [
        '#type' => 'button',
        '#value' => $this->t('Refetch oEmbed Resource'),
        '#ajax' => [
          'callback' => [MediaEntityFormHandler::class, 'submitRefetch'],
          'disable-refocus' => FALSE,
          'event' => 'click',
          'wrapper' => 'cache-timestamp',
          'progress' => [
            'type' => 'throbber',
            'message' => NULL,
          ],
        ],
      ];
    }
  }

  /**
   * Refetches the oEmbed resource for the media entity.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function submitRefetch(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\oembed_resource\HelperInterface $helper */
    $helper = \Drupal::service('oembed_resource.helper');

    /** @var \Drupal\Core\Form\FormInterface $callback_object */
    $callback_object = $form_state->getFormObject();
    /** @var \Drupal\media\MediaInterface $entity */
    $entity = $callback_object->getEntity();
    $helper->refetchResource($entity);

    if ($timestamp = $helper->getCacheTimestampFromEntity($entity)) {
      /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
      $date_formatter = \Drupal::service('date.formatter');
      /** @var \Drupal\Core\Session\AccountProxyInterface $account */
      $account = \Drupal::service('current_user');

      $cache_created = $date_formatter->format($timestamp, 'short', '', $account->getTimeZone());

      $helper->invalidateMediaEntityCacheTags([$entity->id()]);
      return ['#markup' => $cache_created];
    }
    return ['#markup' => $this->t('Refetch failed.')];
  }

}
