<?php

namespace Drupal\oembed_resource;

use Drupal\media\Entity\Media;

/**
 * The HelperInterface interface.
 */
interface HelperInterface {

  /**
   * Determines if a media entity uses an oEmbed media source.
   *
   * @param \Drupal\media\Entity\Media $entity
   *   A media entity.
   *
   * @return bool
   *   Whether or not a media entity uses an oEmbed media source.
   */
  public function isOembed(Media $entity): bool;

  /**
   * Get the URL for the resource from the media entity.
   *
   * E.g. https://provider.com/asset/123.
   *
   * @param \Drupal\media\Entity\Media $entity
   *   A media entity.
   *
   * @return string
   *   The resource URL corresponding to the given media item URL.
   */
  public function getUrlFromEntity(Media $entity): string;

  /**
   * Get the oEmbed resource URL from the media entity.
   *
   * The "resource" URL is the URL to which oEmbed requests are made.
   * E.g. https://provider.com/oembed?url=https://provider.com/asset/123.
   *
   * @param \Drupal\media\Entity\Media $entity
   *   A media entity.
   *
   * @return string
   *   The resource URL corresponding to the given media item URL.
   */
  public function getResourceUrlFromEntity(Media $entity): string;

  /**
   * Returns the cache ID for a resource given a media entity.
   *
   * @param \Drupal\media\Entity\Media $entity
   *   A media entity.
   *
   * @return string
   *   The cache ID.
   */
  public function getResourceCacheIdFromEntity(Media $entity): string;

  /**
   * Returns resource URL cache IDs given a media entity.
   *
   * @param \Drupal\media\Entity\Media $entity
   *   A media entity.
   *
   * @return array
   *   An array of cache IDs.
   */
  public function getResourceUrlCacheIdsFromEntity(Media $entity): array;

  /**
   * Gets the cache timestamp from the entity.
   *
   * @return mixed
   *   string|false The cache item timestamp or FALSE on failure.
   */
  public function getCacheTimestampFromEntity(Media $entity): mixed;

  /**
   * Deletes and refetches a cached resource given a media entity.
   *
   * @param \Drupal\media\Entity\Media $entity
   *   A media entity.
   */
  public function refetchResource(Media $entity): void;

  /**
   * Deletes cache items relating to an oEmbed resource given a media entity.
   *
   * @param \Drupal\media\Entity\Media $entity
   *   A media entity.
   */
  public function deleteResourceCacheItems(Media $entity): void;

  /**
   * Invalidates media entity cachetags.
   *
   * @param array $media_ids
   *   An array of media entity IDs.
   */
  public function invalidateMediaEntityCacheTags(array $media_ids): void;

  /**
   * Invalidates cache for all oEmbed media enties.
   */
  public function invalidateAllOembedMediaEntities(): void;

}
