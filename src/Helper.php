<?php

namespace Drupal\oembed_resource;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\Entity\Media;
use Drupal\media\OEmbed\ResourceFetcherInterface;
use Drupal\media\OEmbed\UrlResolverInterface;
use Drupal\media\Plugin\media\Source\OEmbed;
use Drupal\pcb\Cache\PermanentBackendInterface;

/**
 * The Helper service.
 */
class Helper implements HelperInterface {

  /**
   * The oEmbed resource cache backend.
   *
   * @var \Drupal\pcb\Cache\PermanentBackendInterface
   */
  protected $cache;

  /**
   * Converts oEmbed media URLs into endpoint-specific resource URLs.
   *
   * @var \Drupal\media\OEmbed\UrlResolverInterface
   */
  protected $urlResolver;

  /**
   * Fetches and caches oEmbed resources.
   *
   * @var \Drupal\media\OEmbed\ResourceFetcherInterface
   */
  protected $resourceFetcher;

  /**
   * The cache tag invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagInvalidator;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Provides discovery and retrieval of entity type bundles.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Manages entity type plugin definitions.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MediaEntityFormHandler constructor.
   *
   * @param \Drupal\pcb\Cache\PermanentBackendInterface $cache
   *   The oEmbed resource cache backend.
   * @param \Drupal\media\OEmbed\UrlResolverInterface $url_resolver
   *   Converts oEmbed media URLs into endpoint-specific resource URLs.
   * @param \Drupal\media\OEmbed\ResourceFetcherInterface $resource_fetcher
   *   Fetches and caches oEmbed resources.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tag_invalidator
   *   The cache tag invalidator service.
   * @param \Drupal\Core\Database\Connection $database
   *   The current active database's master connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   Provides discovery and retrieval of entity type bundles.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Manages entity type plugin definitions.
   */
  public function __construct(PermanentBackendInterface $cache, UrlResolverInterface $url_resolver, ResourceFetcherInterface $resource_fetcher, CacheTagsInvalidatorInterface $cache_tag_invalidator, Connection $database, ConfigFactoryInterface $config, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager) {
    $this->cache = $cache;
    $this->urlResolver = $url_resolver;
    $this->resourceFetcher = $resource_fetcher;
    $this->cacheTagInvalidator = $cache_tag_invalidator;
    $this->database = $database;
    $this->config = $config;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function isOembed(Media $entity): bool {
    /** @var \Drupal\media\MediaSourceInterface $source */
    $source = $entity->getSource();
    return ($source instanceof OEmbed) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlFromEntity(Media $entity): string {
    /** @var \Drupal\media\MediaSourceInterface $source */
    $source = $entity->getSource();
    return $source->getSourceFieldValue($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceUrlFromEntity(Media $entity): string {
    $url = $this->getUrlFromEntity($entity);
    return $this->urlResolver->getResourceUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceCacheIdFromEntity(Media $entity): string {
    $resource_url = $this->getResourceUrlFromEntity($entity);
    return "media:oembed_resource:$resource_url";
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceUrlCacheIdsFromEntity(Media $entity): array {
    $cache_id_prefix = 'media:oembed_resource_url:' . $this->getUrlFromEntity($entity) . ':';

    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query = $this->database->select('cache_oembed_resource', 'c')
      ->condition('c.cid', $cache_id_prefix . '%', 'LIKE')
      ->fields('c', ['cid']);
    /** @var \Drupal\Core\Database\StatementInterface $results */
    $results = $query->execute();
    $return = [];
    foreach ($results as $result) {
      $return[] = $result->cid;
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTimestampFromEntity(Media $entity): mixed {
    $cache_id = $this->getResourceCacheIdFromEntity($entity);
    if ($cache_item = $this->cache->get($cache_id)) {
      return $cache_item->created;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function refetchResource(Media $entity): void {
    $this->deleteResourceCacheItems($entity);
    $resource_url = $this->getResourceUrlFromEntity($entity);
    $this->resourceFetcher->fetchResource($resource_url);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteResourceCacheItems(Media $entity): void {
    $cache_ids[] = $this->getResourceCacheIdFromEntity($entity);
    foreach ($this->getResourceUrlCacheIdsFromEntity($entity) as $cid) {
      $cache_ids[] = $cid;
    }

    $this->cache->deleteMultiple($cache_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateMediaEntityCacheTags(array $media_ids): void {
    $tags = [];
    foreach ($media_ids as $media_id) {
      $tags[] = "media:$media_id";
    }
    $this->cacheTagInvalidator->invalidateTags($tags);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateAllOembedMediaEntities(): void {
    $entity_ids = [];
    foreach ($this->entityTypeBundleInfo->getBundleInfo('media') as $id => $name) {
      /** @var \Drupal\media\MediaSourceInterface $source */
      $source = $this->config->get("media.type.$id")->get('source');
      if (str_starts_with($source, 'oembed:')) {
        $media_entities = $this->entityTypeManager
          ->getStorage('media')
          ->loadByProperties(['bundle' => $id]);

        foreach ($media_entities as $media_entity) {
          $entity_ids[] = $media_entity->id();
        }
      }

    }
    if ($entity_ids) {
      $this->invalidateMediaEntityCacheTags($entity_ids);
    }
  }

}
