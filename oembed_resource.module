<?php

/**
 * @file
 * The module file.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\media\MediaForm;
use Drupal\oembed_resource\MediaEntityFormHandler;
use Drupal\oembed_resource\PerformanceFormHandler;
use Drupal\system\Form\PerformanceForm;

/**
 * Implements hook_form_alter().
 */
function oembed_resource_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\Core\Form\FormInterface $callback_object */
  $callback_object = $form_state->getFormObject();
  if ($callback_object instanceof MediaForm) {
    return \Drupal::service('class_resolver')
      ->getInstanceFromDefinition(MediaEntityFormHandler::class)
      ->alterForm($form, $form_state, $form_id);
  }
  if ($callback_object instanceof PerformanceForm) {
    return \Drupal::service('class_resolver')
      ->getInstanceFromDefinition(PerformanceFormHandler::class)
      ->alterForm($form, $form_state, $form_id);
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function oembed_resource_module_implements_alter(&$implementations, $hook) {
  switch ($hook) {
    // Move our hook_form_alter() implementation to the end of the list.
    case 'form_alter':
      $group = $implementations['oembed_resource'];
      unset($implementations['oembed_resource']);
      $implementations['oembed_resource'] = $group;
      break;
  }
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function oembed_resource_media_delete(EntityInterface $entity) {
  /** @var \Drupal\oembed_resource\HelperInterface $helper */
  $helper = \Drupal::service('oembed_resource.helper');
  if ($helper->isOembed($entity)) {
    $helper->deleteResourceCacheItems($entity);
  }
}
